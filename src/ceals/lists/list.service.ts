import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { List } from './entities/list.entity';
import {
  AllListsResponseDto,
  CreateListRequestDto,
  DeleteListRequestDto,
  DeleteListResponseDto,
  ElectionIdRequestDto,
  GetListRequestDto,
  ListResponseDto,
  UpdateListRequestDto,
} from './dto/list.dto';

@Injectable()
export class ListService {
  constructor(
    @InjectRepository(List)
    private readonly listRepository: Repository<List>,
  ) {}

  async createList(data: CreateListRequestDto): Promise<ListResponseDto> {
    const list = this.listRepository.create(data);
    await this.listRepository.save(list);
    return list;
  }

  async getAllLists(): Promise<AllListsResponseDto> {
    const lists = await this.listRepository.find();
    return { lists };
  }

  async getList(data: GetListRequestDto): Promise<ListResponseDto> {
    const list = await this.listRepository.findOne({ where: { id: data.id } });
    if (!list) {
      throw new NotFoundException(`List with ID ${data.id} not found`);
    }
    return list;
  }

  async updateList(data: UpdateListRequestDto): Promise<ListResponseDto> {
    const list = await this.getList({ id: data.id });
    this.listRepository.merge(list, data);
    await this.listRepository.save(list);
    return list;
  }

  async deleteList(data: DeleteListRequestDto): Promise<DeleteListResponseDto> {
    const result = await this.listRepository.delete(data.id);
    if (result.affected === 0) {
      throw new NotFoundException(`List with ID ${data.id} not found`);
    }
    return { success: true };
  }

  async getListsByElectionId(data: ElectionIdRequestDto): Promise<AllListsResponseDto> {
    const lists = await this.listRepository.find({ where: { electionId: data.electionId } });
    return { lists };
  }
}

