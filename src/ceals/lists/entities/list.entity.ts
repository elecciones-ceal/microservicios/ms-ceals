import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
//import { Integrant } from '../integrants/integrant.entity';

@Entity()
export class List {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  electionId: number;
}
