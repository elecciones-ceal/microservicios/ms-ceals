import { IsInt, IsString } from 'class-validator';
import {
  CreateListRequest,
  UpdateListRequest,
  GetListRequest,
  DeleteListRequest,
  ElectionIdRequest,
  ListResponse,
  AllListsResponse,
  DeleteListResponse,
} from 'src/ceals/ceal.pb';

export class CreateListRequestDto implements CreateListRequest {
  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsInt()
  electionId: number;
}

export class UpdateListRequestDto implements UpdateListRequest {
  @IsInt()
  id: number;

  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsInt()
  electionId: number;
}

export class GetListRequestDto implements GetListRequest {
  @IsInt()
  id: number;
}

export class DeleteListRequestDto implements DeleteListRequest {
  @IsInt()
  id: number;
}

export class ElectionIdRequestDto implements ElectionIdRequest {
  @IsInt()
  electionId: number;
}

export class ListResponseDto implements ListResponse {
  @IsInt()
  id: number;

  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsInt()
  electionId: number;
}

export class AllListsResponseDto implements AllListsResponse {
  lists: ListResponseDto[];
}

export class DeleteListResponseDto implements DeleteListResponse {
  success: boolean;
}
