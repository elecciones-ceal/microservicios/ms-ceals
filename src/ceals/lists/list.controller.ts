import { Controller, NotFoundException } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { ListService } from './list.service';
import {
  AllListsResponseDto,
  CreateListRequestDto,
  DeleteListRequestDto,
  DeleteListResponseDto,
  ElectionIdRequestDto,
  GetListRequestDto,
  ListResponseDto,
  UpdateListRequestDto,
} from './dto/list.dto';

@Controller()
export class ListController {
  constructor(private readonly listService: ListService) {}

  @GrpcMethod('ListService', 'CreateList')
  createList(data: CreateListRequestDto): Promise<ListResponseDto> {
    return this.listService.createList(data);
  }

  @GrpcMethod('ListService', 'GetAllLists')
  getAllLists(): Promise<AllListsResponseDto> {
    return this.listService.getAllLists();
  }

  @GrpcMethod('ListService', 'GetList')
  async getList(data: GetListRequestDto): Promise<ListResponseDto> {
    const list = await this.listService.getList(data);
    if (!list) {
      throw new NotFoundException(`List with ID ${data.id} not found`);
    }
    return list;
  }

  @GrpcMethod('ListService', 'UpdateList')
  updateList(data: UpdateListRequestDto): Promise<ListResponseDto> {
    return this.listService.updateList(data);
  }

  @GrpcMethod('ListService', 'DeleteList')
  deleteList(data: DeleteListRequestDto): Promise<DeleteListResponseDto> {
    return this.listService.deleteList(data);
  }

  @GrpcMethod('ListService', 'GetListsByElectionId')
  getListsByElectionId(
    data: ElectionIdRequestDto,
  ): Promise<AllListsResponseDto> {
    return this.listService.getListsByElectionId(data);
  }
}
