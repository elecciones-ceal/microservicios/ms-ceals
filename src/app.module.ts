import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { List } from './ceals/lists/entities/list.entity';
import { ListController } from './ceals/lists/list.controller';
import { ListService } from './ceals/lists/list.service';

import * as dotenv from 'dotenv';
import { ListsModule } from './ceals/lists/list.module';

dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    ListsModule,
  ],
})
export class AppModule {}
